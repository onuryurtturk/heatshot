package com.onuryurtturk.heatshot.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User (

    @SerializedName("name")
    val name: String = "",
    @SerializedName("surname")
    val surname: String = ""

) : Serializable