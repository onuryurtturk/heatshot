package com.onuryurtturk.heatshot.ui

import android.os.Bundle
import androidx.annotation.AnyThread
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.onuryurtturk.heatshot.R
import com.onuryurtturk.heatshot.databinding.ActivityShotAnalyseBinding
import com.onuryurtturk.heatshot.helper.HeatMap
import com.onuryurtturk.heatshot.helper.HeatMapMarkerCallback
import com.onuryurtturk.heatshot.model.HeatShot
import com.onuryurtturk.heatshot.viewmodel.HeatShotViewModel
import kotlinx.android.synthetic.main.activity_shot_analyse.*
import org.eazegraph.lib.models.PieModel


class HeatShotAnalyseActivity : AppCompatActivity() {

    /**
     * variables
     */
    private lateinit var dataBinding : ActivityShotAnalyseBinding
    private lateinit var viewModel : HeatShotViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding= DataBindingUtil.setContentView(this, R.layout.activity_shot_analyse)
        viewModel = ViewModelProviders.of(this).get(HeatShotViewModel::class.java)
        viewModel.selectedHeatShot.value= intent.getSerializableExtra("SelectedShot") as HeatShot
        initMap()
    }

    /**
     * init heatmap function
     * add data to map
     */
    private fun initMap(){
        val sb = StringBuilder()
        sb.append(viewModel.selectedHeatShot.value?.user?.name).append(viewModel.selectedHeatShot.value?.user?.surname)
        userInfo.text = sb.toString()
        shotMap.setMinimum(0.0)
        shotMap.setMaximum(100.0)
        shotMap.setMarkerCallback(HeatMapMarkerCallback.CircleHeatMapMarker(ContextCompat.getColor(this, R.color.colorAccent)))
        shotMap.setRadius(0.000001)
        addData()
        initChart()
    }

    private fun initChart(){
        pieChart.addPieSlice(PieModel(resources.getString(R.string.success_rate), viewModel.calculatePlayerRate().toFloat(), getColor(R.color.colorAccent)))
        pieChart.startAnimation()
    }

    /**
     *  add data to map
     */
    private fun addData() {
        drawNewMap()
        shotMap.forceRefresh()
    }

    /**
     *  draw on the map
     */
    @AnyThread
    private fun drawNewMap() {
        shotMap.clearData()
        val dataPoints : List<HeatMap.DataPoint> = viewModel.calculateDataPoints()
        for(item in dataPoints){
            shotMap.addData(item)
        }
    }
}