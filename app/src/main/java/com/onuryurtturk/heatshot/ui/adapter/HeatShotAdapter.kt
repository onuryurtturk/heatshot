package com.onuryurtturk.heatshot.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.onuryurtturk.heatshot.R
import com.onuryurtturk.heatshot.databinding.ItemHeatshotRowBinding
import com.onuryurtturk.heatshot.model.HeatShot
import com.onuryurtturk.heatshot.ui.HeatShotAnalyseActivity

class HeatShotAdapter(val heatshotList: ArrayList<HeatShot>, val mContext : Context) : RecyclerView.Adapter<HeatShotAdapter.HeatShotViewHolder>()  {

    class HeatShotViewHolder(var view: ItemHeatshotRowBinding) : RecyclerView.ViewHolder(view.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeatShotViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ItemHeatshotRowBinding>(inflater, R.layout.item_heatshot_row,parent,false)
        return HeatShotViewHolder(view)
    }

    override fun getItemCount(): Int {
        return heatshotList.size
    }

    override fun onBindViewHolder(holder: HeatShotViewHolder, position: Int) {
        holder.view.heatShot = heatshotList[position]
        holder.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext,HeatShotAnalyseActivity::class.java)
            intent.putExtra("SelectedShot",holder.view.heatShot)
            mContext.startActivity(intent)
        })
    }

    fun updateHeatShotList(shotList:List<HeatShot>){
        heatshotList.clear()
        heatshotList.addAll(shotList)
        notifyDataSetChanged()
    }

}