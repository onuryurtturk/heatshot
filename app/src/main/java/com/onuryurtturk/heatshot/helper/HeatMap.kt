/*
 * HeatMap.java
 *
 * Copyright 2020 Heartland Software Solutions Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the license at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.onuryurtturk.heatshot.helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.annotation.AnyThread
import androidx.annotation.ColorInt
import androidx.annotation.WorkerThread
import com.onuryurtturk.heatshot.R
import java.util.*

/**
 * A class for rendering a heat map in an Android view.
 */
class HeatMap : View, OnTouchListener {
    /**
     * The data that will be displayed in the heat map.
     */
    private var data: MutableList<DataPoint>? =
        null

    /**
     * A buffer for new data that hasn't been displayed yet.
     */
    private var dataBuffer: MutableList<DataPoint>? =
        null

    /**
     * Whether the information stored in dataBuffer has changed.
     */
    private var dataModified = false

    /**
     * The value that corresponds to the minimum of the gradient scale.
     */
    private var min = Double.NEGATIVE_INFINITY

    /**
     * The value that corresponds to the maximum of the gradient scale.
     */
    private var max = Double.POSITIVE_INFINITY

    /**
     * The amount of blur to use.
     */
    private var mBlur = 0.85

    /**
     * The radius (px) of the circle each data point takes up.
     */
    private var mRadius = 200.0

    /**
     * If greater than 0 this will be used as the transparency for the entire map.
     */
    private var opacity = 0

    /**
     * The minimum opacity to use in the map. Only used when [HeatMap.opacity] is 0.
     */
    private var minOpacity = 0

    /**
     * The maximum opacity to use in the map. Only used when [HeatMap.opacity] is 0.
     */
    private var maxOpacity = 255

    /**
     * The bounds of actual data. For the sake of efficiency this stops us updating outside
     * of where data is present.
     */
    private val mRenderBoundaries = DoubleArray(4)

    /**
     * Colors to be used in building the gradient.
     */
    @ColorInt
    private var colors = intArrayOf(-0x10000, -0xff0100)

    /**
     * The stops to position the colors at.
     */
    private var positions = floatArrayOf(0.0f, 1.0f)

    /**
     * A paint for solid black.
     */
    private var mBlack: Paint? = null
    private var mTransparentBackground = true

    /**
     * A paint for the background fill.
     */
    private var mBackground: Paint? = null

    /**
     * A paint to be used to fill objects.
     */
    private var mFill: Paint? = null

    /**
     * The color palette being used to create the radial gradients.
     */
    private var palette: IntArray? = null

    /**
     * Whether the palette needs refreshed.
     */
    private var needsRefresh = true

    /**
     * Update the shadow layer when the size changes.
     */
    private var sizeChange = false

    /**
     * The top padding on the heatmap.
     */
    private var mTop = 0f

    /**
     * The left padding on the heatmap.
     */
    private var mLeft = 0f

    /**
     * The right padding on the heatmap.
     */
    private var mRight = 0f

    /**
     * The bottom padding on the heatmap.
     */
    private var mBottom = 0f

    /**
     * The maximum width of the rendering surface.
     */
    private var mMaxWidth: Int? = 0

    /**
     * The maximum height of the rendering surface.
     */
    private var mMaxHeight: Int? = 0

    /**
     * The aspect ratio scale.
     */
    private var mScale: Float? = null

    /**
     * A listener for click events.
     */
    private var mListener: OnMapClickListener? = null

    /**
     * The bitmap that the shadow layer is rendered into.
     */
    private var mShadow: Bitmap? = null

    /**
     * A lock to make sure that the bitmap is not rendered more than once at a time.
     */
    private val tryRefreshLock = Any()

    /**
     * Should the drawing cache be used or should a new bitmap be created.
     */
    private var mUseDrawingCache = false

    /**
     * A listener that is used to draw
     */
    private var mMarkerCallback: HeatMapMarkerCallback? = null

    /**
     * Set a right padding for the data positions. The gradient will still extend into the
     * padding area.
     * @param padding The amount of padding to add to the right of the data points (in pixels).
     */
    fun setRightPadding(padding: Int) {
        mRight = padding.toFloat()
    }

    /**
     * Set a left padding for the data positions. The gradient will still extend into the
     * padding area.
     * @param padding The amount of padding to add to the left of the data points (in pixels).
     */
    fun setLeftPadding(padding: Int) {
        mLeft = padding.toFloat()
    }

    /**
     * Set a top padding for the data positions. The gradient will still extend into the
     * padding area.
     * @param padding The amount of padding to add to the top of the data points (in pixels).
     */
    fun setTopPadding(padding: Int) {
        mTop = padding.toFloat()
    }

    /**
     * Set a bottom padding for the data positions. The gradient will still extend into the
     * padding area.
     * @param padding The amount of padding to add to the bottom of the data points (in pixels).
     */
    fun setBottomPadding(padding: Int) {
        mBottom = padding.toFloat()
    }

    /**
     * Show markers at the data positions.
     * @param callback Callback that will draw the data point markers.
     */
    fun setMarkerCallback(callback: HeatMapMarkerCallback?) {
        mMarkerCallback = callback
    }

    /**
     * Get the heat map's blur factor.
     */
    /**
     * Set the blur factor for the heat map. Must be between 0 and 1.
     * @param blur The blur factor
     */
    @get:AnyThread
    @set:AnyThread
    var blur: Double
        get() = mBlur
        set(blur) {
            require(!(blur > 1.0 || blur < 0.0)) { "Blur must be between 0 and 1." }
            mBlur = blur
        }

    /**
     * Sets the value associated with the maximum on the gradient scale.
     *
     * This should be greater than the minimum value.
     * @param max The maximum value.
     */
    @AnyThread
    fun setMaximum(max: Double) {
        this.max = max
    }

    /**
     * Sets the value associated with the minimum on the gradient scale.
     *
     * This should be less than the maximum value.
     * @param min The minimum value.
     */
    @AnyThread
    fun setMinimum(min: Double) {
        this.min = min
    }

    /**
     * Set the opacity to be used in the heat map. This opacity will be used for the entire map.
     * @param opacity The opacity in the range [0,255].
     */
    @AnyThread
    fun setOpacity(opacity: Int) {
        this.opacity = opacity
    }

    /**
     * Set the minimum opacity to be used in the map. Only used when [HeatMap.opacity] is 0.
     * @param min The minimum opacity in the range [0,255].
     */
    @AnyThread
    fun setMinimumOpacity(min: Int) {
        minOpacity = min
    }

    /**
     * Set the maximum opacity to be used in the map. Only used when [HeatMap.opacity] is 0.
     * @param max The maximum opacity in the range [0,255].
     */
    @AnyThread
    fun setMaximumOpacity(max: Int) {
        maxOpacity = max
    }

    /**
     * Set the circles radius when drawing data points.
     * @param radius The radius in pixels.
     */
    @AnyThread
    fun setRadius(radius: Double) {
        mRadius = radius
    }

    /**
     * Use the drawing cache instead of creating a new [Bitmap]. Causes [NullPointerException] on some
     * devices so is disabled by default.
     * @param use Use the drawing cache instead of a new [Bitmap].
     */
    fun setUseDrawingCache(use: Boolean) {
        mUseDrawingCache = use
        invalidate()
    }

    /**
     * The maximum width of the bitmap that is used to render the heatmap.
     * @param width The maximum width in pixels.
     */
    fun setMaxDrawingWidth(width: Int) {
        mMaxWidth = width
        mScale = null
    }

    /**
     * The maximum height of the bitmap that is used to render the heatmap.
     * @param height The maximum height in pixels.
     */
    fun setMaxDrawingHeight(height: Int) {
        mMaxHeight = height
        mScale = null
    }

    /**
     * Set the color stops used for the heat map's gradient. There needs to be at least 2 stops
     * and there should be one at a position of 0 and one at a position of 1.
     * @param stops A map from stop positions (as fractions of the width in [0,1]) to ARGB colors.
     */
    @AnyThread
    fun setColorStops(stops: Map<Float, Int?>) {
        require(stops.size >= 2) { "There must be at least 2 color stops" }
        colors = IntArray(stops.size)
        positions = FloatArray(stops.size)
        var i = 0
        for (key in stops.keys) {
            colors[i] = stops[key]!!
            positions[i] = key
            i++
        }
        if (!mTransparentBackground) mBackground!!.color = colors[0]
    }

    /**
     * Add a new data point to the heat map.
     *
     * Does not refresh the display. See [HeatMap.forceRefresh] in order to redraw the heat map.
     * @param point A new data point.
     */
    @AnyThread
    fun addData(point: DataPoint) {
        dataBuffer!!.add(point)
        dataModified = true
    }

    /**
     * Clears the data that is being displayed in the heat map.
     *
     * Does not refresh the display. See [HeatMap.forceRefresh] in order to redraw the heat map.
     */
    @AnyThread
    fun clearData() {
        dataBuffer!!.clear()
        dataModified = true
    }

    /**
     * Register a callback to be invoked when this view is clicked. It will return the closest
     * data point as well as the clicked location.
     * @param listener The callback that will run
     */
    fun setOnMapClickListener(listener: OnMapClickListener?) {
        mListener = listener
    }

    /**
     * Register a callback to be invoked when this view is touched.
     * @param listener The callback that will run
     */
    @Deprecated("Use {@link #setOnMapClickListener(OnMapClickListener)} instead.")
    override fun setOnTouchListener(listener: OnTouchListener) {
        mListener = null
        super.setOnTouchListener(listener)
    }

    /**
     * Simple constructor to use when creating a view from code.
     * @param context The Context the view is running in, through which it can access the current theme, resources, etc.
     */
    constructor(context: Context?) : super(context) {
        initialize()
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called when a view is
     * being constructed from an XML file, supplying attributes that were specified in the XML file.
     * This version uses a default style of 0, so the only attribute values applied are those in the
     * Context's Theme and the given AttributeSet.
     * @param context The Context the view is running in, through which it can access the current theme, resources, etc.
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        initialize()
        val a =
            context.theme.obtainStyledAttributes(attrs, R.styleable.HeatMap, 0, 0)
        try {
            opacity = a.getInt(R.styleable.HeatMap_opacity, -1)
            if (opacity < 0) opacity = 0
            minOpacity = a.getInt(R.styleable.HeatMap_minOpacity, -1)
            if (minOpacity < 0) minOpacity = 0
            maxOpacity = a.getInt(R.styleable.HeatMap_maxOpacity, -1)
            if (maxOpacity < 0) maxOpacity = 255
            mBlur = a.getFloat(R.styleable.HeatMap_blur, -1f).toDouble()
            if (mBlur < 0) mBlur = 0.85
            mRadius = a.getDimension(R.styleable.HeatMap_radius, -1f).toDouble()
            if (mRadius < 0) mRadius = 200.0
            var padding = a.getDimension(R.styleable.HeatMap_dataPadding, -1f)
            if (padding < 0) padding = 0f
            mTop = a.getDimension(R.styleable.HeatMap_dataPaddingTop, -1f)
            if (mTop < 0) mTop = padding
            mBottom = a.getDimension(R.styleable.HeatMap_dataPaddingBottom, -1f)
            if (mBottom < 0) mBottom = padding
            mRight = a.getDimension(R.styleable.HeatMap_dataPaddingRight, -1f)
            if (mRight < 0) mRight = padding
            mLeft = a.getDimension(R.styleable.HeatMap_dataPaddingLeft, -1f)
            if (mLeft < 0) mLeft = padding
            mMaxWidth = a.getDimension(R.styleable.HeatMap_maxDrawingWidth, -1f).toInt()
            if (mMaxWidth!! < 0) mMaxWidth = null
            mMaxHeight = a.getDimension(R.styleable.HeatMap_maxDrawingHeight, -1f).toInt()
            if (mMaxHeight!! < 0) mMaxHeight = null
            mTransparentBackground = a.getBoolean(R.styleable.HeatMap_transparentBackground, true)
        } finally {
            a.recycle()
        }
    }

    /**
     * Force a refresh of the heat map.
     *
     * Use this instead of [View.invalidate].
     */
    fun forceRefresh() {
        needsRefresh = true
        invalidate()
    }

    /**
     * Initialize all of the paints that we're cable of before drawing.
     */
    private fun initialize() {
        mBlack = Paint()
        mBlack!!.color = -0x1000000
        mFill = Paint()
        mFill!!.style = Paint.Style.FILL
        mBackground = Paint()
        if (!mTransparentBackground) mBackground!!.color = -0x10102
        data = ArrayList()
        dataBuffer = ArrayList()
        super.setOnTouchListener(this)
        if (mUseDrawingCache) {
            this.isDrawingCacheEnabled = true
            this.drawingCacheBackgroundColor = Color.TRANSPARENT
        }
    }

    @get:SuppressLint("WrongThread")
    @get:AnyThread
    private val drawingWidth: Int
        private get() = if (mMaxWidth == null) width else Math.min(calcMaxWidth(), width)

    @get:SuppressLint("WrongThread")
    @get:AnyThread
    private val drawingHeight: Int
        private get() = if (mMaxHeight == null) height else Math.min(calcMaxHeight(), height)

    @get:AnyThread
    private val scale: Float
        private get() {
            if (mScale == null) {
                mScale = if (mMaxWidth == null || mMaxHeight == null) 1.0f else {
                    val sourceRatio = width / height.toFloat()
                    val targetRatio = mMaxWidth!! / mMaxHeight!!.toFloat()
                    if (sourceRatio < targetRatio) {
                        (width / mMaxWidth!!).toFloat()
                    } else {
                        (height / mMaxHeight!!).toFloat()
                    }
                }
            }
            return mScale as Float
        }

    @AnyThread
    @SuppressLint("WrongThread")
    private fun calcMaxHeight(): Int {
        return (height / scale).toInt()
    }

    @AnyThread
    @SuppressLint("WrongThread")
    private fun calcMaxWidth(): Int {
        return (width / scale).toInt()
    }

    @AnyThread
    @SuppressLint("WrongThread")
    private fun redrawShadow(width: Int, height: Int) {

        mRenderBoundaries[0] = 10000.0
        mRenderBoundaries[1] = 10000.0
        mRenderBoundaries[2] = 0.0
        mRenderBoundaries[3] = 0.0
        mShadow = if (mUseDrawingCache) drawingCache else Bitmap.createBitmap(
            drawingWidth,
            drawingHeight,
            Bitmap.Config.ARGB_8888
        )
        val shadowCanvas = Canvas(mShadow!!)
        drawTransparent(shadowCanvas, width, height)
    }

    /**
     * Draws the heatmap from a background thread.
     *
     * This allows offloading some of the work that would usualy be done in
     * [.onDraw] into a background thread. If the view is redrawn
     * for some reason while this operation is still ongoing, the UI thread
     * will block until this call is finished.
     *
     * The caller should take care to invalidate the view on the UI thread
     * afterwards, but not before this call has finished.
     *
     * <pre>`final HeatMap heatmap = (HeatMap) findViewById(R.id.heatmap);
     * new AsyncTask<Void,Void,Void>() {
     * protected Void doInBackground(Void... params) {
     * Random rand = new Random();
     * //add 20 random points of random intensity
     * for (int i = 0; i < 20; i++) {
     * heatmap.addData(getRandomDataPoint());
     * }
     *
     * heatmap.forceRefreshOnWorkerThread();
     *
     * return null;
     * }
     *
     * protected void onPostExecute(Void aVoid) {
     * heatmap.invalidate();
     * heatmap.setAlpha(0.0f);
     * heatmap.animate().alpha(1.0f).setDuration(700L).start();
     * }
     * }.execute();
    `</pre> *
     */
    @WorkerThread
    @SuppressLint("WrongThread")
    fun forceRefreshOnWorkerThread() {
        synchronized(tryRefreshLock) {
            // These getters are in fact available on this thread. The caller will have to
            // take care that the view is in an acceptable state here.
            tryRefresh(true, drawingWidth, drawingHeight)
        }
    }

    /**
     * If needed, refresh the palette.
     */
    @AnyThread
    private fun tryRefresh(forceRefresh: Boolean, width: Int, height: Int) {
        if (forceRefresh || needsRefresh) {
            val bit = Bitmap.createBitmap(256, 1, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bit)
            val grad: LinearGradient = LinearGradient(
                0f,
                0f,
                256f,
                1f,
                colors,
                positions,
                Shader.TileMode.CLAMP
            )
            val paint = Paint()
            paint.style = Paint.Style.FILL
            paint.shader = grad
            canvas.drawLine(0f, 0f, 256f, 1f, paint)
            palette = IntArray(256)
            bit.getPixels(palette, 0, 256, 0, 0, 256, 1)
            if (dataModified) {
                data!!.clear()
                data!!.addAll(dataBuffer!!)
                dataBuffer!!.clear()
                dataModified = false
            }
            redrawShadow(width, height)
        } else if (sizeChange) {
            redrawShadow(width, height)
        }
        needsRefresh = false
        sizeChange = false
    }

    public override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (mMaxWidth == null || mMaxHeight == null) sizeChange = true
    }

    /**
     * Draw the heat map.
     *
     * @param canvas Canvas to draw into.
     */
    override fun onDraw(canvas: Canvas) {
        synchronized(tryRefreshLock) { tryRefresh(false, drawingWidth, drawingHeight) }
        drawColour(canvas)
    }

    /**
     * Draw a radial gradient at a given location. Only draws in black with the gradient being only
     * in transparency.
     *
     * @param canvas Canvas to draw into.
     * @param x The x location to draw the point.
     * @param y The y location to draw the point.
     * @param radius The radius (in pixels) of the point.
     * @param blurFactor A factor to scale the circles width by.
     * @param alpha The transparency of the gradient.
     */
    @AnyThread
    private fun drawDataPoint(canvas: Canvas, x: Float, y: Float, radius: Double, blurFactor: Double, alpha: Double) {
        if (blurFactor == 1.0) {
            canvas.drawCircle(x, y, radius.toFloat(), mBlack!!)
        } else {
            //create a radial gradient at the requested position with the requested size
            val gradient = RadialGradient(x, y, (radius * blurFactor).toFloat(), intArrayOf(
                    Color.argb((alpha * 255).toInt(), 0, 0, 0),
                    Color.argb(0, 0, 0, 0)
                ),
                null,
                Shader.TileMode.CLAMP
            )
            mFill!!.shader = gradient
            // canvas.drawCircle(x, y, (2 * radius).toFloat(), mFill!!)
            drawPolygon(canvas,x,y,(2 * radius).toFloat(),6f,0f,false, mFill!!)
        }
    }

    private fun drawPolygon(mCanvas: Canvas, x: Float, y: Float, radius: Float, sides: Float, startAngle: Float, anticlockwise: Boolean, paint: Paint) {
        if (sides < 3) {
            return
        }
        val a = Math.PI.toFloat() * 2 / sides * if (anticlockwise) -1 else 1
        mCanvas.save()
        mCanvas.translate(x, y)
        mCanvas.rotate(startAngle)
        val path = Path()
        path.moveTo(radius, 0F)
        var i = 1
        while (i < sides) {
            path.lineTo(radius * Math.cos(a * i.toDouble()).toFloat(), radius * Math.sin(a * i.toDouble()).toFloat())
            i++
        }
        path.close()
        mCanvas.drawPath(path, paint)
        mCanvas.restore()
    }

    /**
     * Draw a heat map in only black and transparency to be used as the blended base of the coloured
     * version.
     *
     * @param canvas Canvas to draw into.
     * @param width The width of the view.
     * @param height The height of the view.
     */
    @AnyThread
    private fun drawTransparent(
        canvas: Canvas,
        width: Int,
        height: Int
    ) {
        //invert the blur factor
        val blur = 1 - mBlur

        //clear the canvas
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        val scale = scale
        val top = mTop / scale
        val bottom = mBottom / scale
        val left = mLeft / scale
        val right = mRight / scale
        val w = width - left - right
        val h = height - top - bottom

        //loop through the data points
        for (point in data!!) {
            val x = point.x * w + left
            val y = point.y * h + top
            val value = Math.max(min, Math.min(point.value, max))
            //the edge of the bounding rectangle for the circle
            val rectX = x - mRadius
            val rectY = y - mRadius

            //calculate the transparency of the circle from its percentage between the max and
            //min values
            val alpha = (value - min) / (max - min)

            //draw the point into the canvas
            drawDataPoint(canvas, x, y, mRadius, blur, alpha)

            //update the modified bounds of the image if necessary
            if (rectX < mRenderBoundaries[0]) mRenderBoundaries[0] = rectX
            if (rectY < mRenderBoundaries[1]) mRenderBoundaries[1] = rectY
            if (rectX + 2 * mRadius > mRenderBoundaries[2]) mRenderBoundaries[2] =
                rectX + 2 * mRadius
            if (rectY + 2 * mRadius > mRenderBoundaries[3]) mRenderBoundaries[3] =
                rectY + 2 * mRadius
        }
    }

    /**
     * Convert the black/transparent heat map into a full colour one.
     *
     * @param canvas The canvas to draw into.
     */
    private fun drawColour(canvas: Canvas) {
        if (data!!.size == 0) return

        //calculate the bounds of shadow layer that have modified pixels
        var x = mRenderBoundaries[0].toInt()
        var y = mRenderBoundaries[1].toInt()
        var width = mRenderBoundaries[2].toInt()
        var height = mRenderBoundaries[3].toInt()
        var maxWidth = drawingWidth
        var maxHeight = drawingHeight
        if (maxWidth > mShadow!!.width && mShadow!!.width != 0) maxWidth = mShadow!!.width
        if (maxHeight > mShadow!!.height && mShadow!!.height != 0) maxHeight =
            mShadow!!.height
        if (x < 0) x = 0
        if (y < 0) y = 0
        if (x + width > maxWidth) width = maxWidth - x
        if (y + height > maxHeight) height = maxHeight - y

        //retrieve the modified pixels from the shadow layer
        val pixels = IntArray(width)

        //loop over each retrieved pixel
        for (j in 0 until height) {
            mShadow!!.getPixels(pixels, 0, width, x, y + j, width, 1)
            for (i in 0 until width) {
                val pixel = pixels[i]
                //the pixels alpha value (0-255)
                val alpha = 0xff and (pixel shr 24)

                //clamp the alpha value to user specified bounds
                var clampAlpha: Int
                clampAlpha = if (opacity > 0) opacity else {
                    if (alpha < maxOpacity) {
                        Math.max(alpha, minOpacity)
                    } else {
                        maxOpacity
                    }
                }

                //set the pixels colour to its corresponding colour in the palette
                pixels[i] = 0xff and clampAlpha shl 24 or (0xffffff and palette!![alpha])
            }

            //set the modified pixels back into the bitmap
            mShadow!!.setPixels(pixels, 0, width, x, y + j, width, 1)
        }

        //clear to the min colour
        if (!mTransparentBackground) canvas.drawRect(
            0f,
            0f,
            getWidth().toFloat(),
            getHeight().toFloat(),
            mBackground!!
        )
        //render the bitmap onto the heat map
        canvas.drawBitmap(
            mShadow!!,
            Rect(0, 0, drawingWidth, drawingHeight),
            Rect(0, 0, getWidth(), getHeight()),
            null
        )

        //draw markers at each data point if requested
        if (mMarkerCallback != null) {
            val rwidth = getWidth() - mLeft - mRight
            val rheight = getHeight() - mTop - mBottom
            for (point in data!!) {
                val rx = point.x * rwidth + mLeft
                val ry = point.y * rheight + mTop
                mMarkerCallback!!.drawMarker(canvas, rx, ry, point)
            }
        }
    }

    private var touchX = 0f
    private var touchY = 0f
    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        if (mListener != null) {
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                var x = motionEvent.x
                var y = motionEvent.y
                val d = Math.sqrt(
                    Math.pow(
                        touchX - x.toDouble(),
                        2.0
                    ) + Math.pow(touchY - y.toDouble(), 2.0)
                )
                if (d < 10) {
                    x = x / width.toFloat()
                    y = y / height.toFloat()
                    var minDist = Double.MAX_VALUE
                    var minPoint: DataPoint? = null
                    for (point in data!!) {
                        val dist = point.distanceTo(x, y)
                        if (dist < minDist) {
                            minDist = dist
                            minPoint = point
                        }
                    }
                    mListener!!.onMapClicked(x.toInt(), y.toInt(), minPoint)
                    return true
                }
            } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                touchX = motionEvent.x
                touchY = motionEvent.y
                return true
            }
        }
        return false
    }

    /**
     * Stores data points to display in the heat map.
     */
    class DataPoint
    /**
     * Construct a new data point to be displayed in the heat map.
     *
     * @param x The data points x location as a decimal percent of the views width
     * @param y The data points y location as a decimal percent of the views height
     * @param value The intensity value of the data point
     */(
        /**
         * The data points x value as a decimal percent of the views width.
         */
        var x: Float,
        /**
         * The data points y value as a decimal percent of the views height.
         */
        var y: Float,
        /**
         * The intensity value of the data point.
         */
        var value: Double,

        var color : Int
    ) {

        /**
         * Any user specific data that may need to be associated with a point.
         */
        var userData: Any? = null
        fun distanceTo(x: Float, y: Float): Double {
            return Math.sqrt(
                Math.pow(
                    x - this.x.toDouble(),
                    2.0
                ) + Math.pow(y - this.y.toDouble(), 2.0)
            )
        }

    }

    interface OnMapClickListener {
        fun onMapClicked(
            x: Int,
            y: Int,
            closest: DataPoint?
        )
    }
}