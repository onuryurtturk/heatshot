package com.onuryurtturk.heatshot.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Shot (

    @SerializedName("point")
    val point: Int = 0,
    @SerializedName("segment")
    val segment: Int = 0,
    @SerializedName("_id")
    val id: String = "",
    @SerializedName("InOut")
    val inOut: Boolean = false,
    @SerializedName("ShotPosX")
    val posX: Double = 0.0,
    @SerializedName("ShotPosY")
    val posY: Double = 0.0

): Serializable