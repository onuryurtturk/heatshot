package com.onuryurtturk.heatshot.network

import io.reactivex.Single
import retrofit2.http.GET

/**
 * Api interface
 */
interface ShotsAPI {

    /**
     * single RXJava Method work 1 time and guarantee result
     */
    @GET("/shots")
    fun getShots():Single<HeatShotResponse>
}