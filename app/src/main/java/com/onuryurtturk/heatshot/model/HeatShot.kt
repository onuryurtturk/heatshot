package com.onuryurtturk.heatshot.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class HeatShot (
    @SerializedName("shots")
    val shots: List<Shot>,
    @SerializedName("user")
    val user: User
): Serializable
