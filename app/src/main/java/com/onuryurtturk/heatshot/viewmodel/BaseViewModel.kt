package com.onuryurtturk.heatshot.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(application: Application):AndroidViewModel(application),CoroutineScope {

    private val job = Job()

    /***
     * First Do Job
     * After complete return to main tread
     */
    override val coroutineContext: CoroutineContext
        get() =job + Dispatchers.Main

    /***
     * If App context finish invoke this
     */
    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}