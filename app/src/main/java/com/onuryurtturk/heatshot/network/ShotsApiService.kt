package com.onuryurtturk.heatshot.network

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ShotsApiService {

    //GET, POST
    private val BASE_URL = "http://ec2-18-188-69-79.us-east-2.compute.amazonaws.com:3000"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(ShotsAPI::class.java)

    fun getData() : Single<HeatShotResponse> {
        return api.getShots()
    }
}