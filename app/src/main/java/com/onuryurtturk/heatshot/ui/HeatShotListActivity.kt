package com.onuryurtturk.heatshot.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.onuryurtturk.heatshot.R
import com.onuryurtturk.heatshot.ui.adapter.HeatShotAdapter
import com.onuryurtturk.heatshot.viewmodel.HeatShotViewModel
import kotlinx.android.synthetic.main.activity_heatshotlist.*

class HeatShotListActivity : AppCompatActivity() {

    private lateinit var viewModel : HeatShotViewModel
    private val shotAdapter = HeatShotAdapter(arrayListOf(),this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_heatshotlist)

        viewModel = ViewModelProviders.of(this).get(HeatShotViewModel::class.java)
        viewModel.getDataFromAPI()

        shotList.layoutManager = LinearLayoutManager(this)
        shotList.adapter = shotAdapter


        swipeRefreshLayout.setOnRefreshListener {
            shotList.visibility = View.GONE
            shotError.visibility = View.GONE
            shotLoading.visibility = View.VISIBLE
            viewModel.getDataFromAPI()
            swipeRefreshLayout.isRefreshing = false

        }

        observeLiveData()
    }

    //view life cycle owner metodunu kullanacaksın
    private fun observeLiveData(){
        viewModel.heatShots.observe(this, Observer { shots ->
            shots?.let {
                shotList.visibility = View.VISIBLE
                shotAdapter.updateHeatShotList(shots)
            }
        })

        viewModel.shotsError.observe(this, Observer { error->
            error?.let {
                if(error){
                    shotError.visibility = View.VISIBLE
                }else{
                    shotError.visibility = View.GONE
                }
            }
        })

        viewModel.shotsLoading.observe(this, Observer {loading->
            loading?.let {
                if(loading){
                    shotLoading.visibility = View.VISIBLE
                    shotList.visibility = View.GONE
                    shotError.visibility = View.GONE
                }else{
                    shotLoading.visibility = View.GONE
                }
            }
        })
    }
}