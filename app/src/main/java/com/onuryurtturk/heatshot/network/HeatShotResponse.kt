package com.onuryurtturk.heatshot.network

import com.google.gson.annotations.SerializedName
import com.onuryurtturk.heatshot.model.HeatShot

data class HeatShotResponse (
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("data")
    val data: List<HeatShot>

)