package com.onuryurtturk.heatshot.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Segment(
    @SerializedName("segmentId")
    val segmentId: Int = 0,
    @SerializedName("color")
    var color: Int = 0,
    @SerializedName("inCount")
    var inCount: Int = 0,
    @SerializedName("outCount")
    var outCount: Int = 0,
    @SerializedName("rate")
    var rate: Double = 0.0
) : Serializable