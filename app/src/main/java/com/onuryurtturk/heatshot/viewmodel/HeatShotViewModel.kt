package com.onuryurtturk.heatshot.viewmodel

import android.app.Application
import android.graphics.Color
import androidx.lifecycle.MutableLiveData
import com.onuryurtturk.heatshot.R
import com.onuryurtturk.heatshot.helper.HeatMap
import com.onuryurtturk.heatshot.model.HeatShot
import com.onuryurtturk.heatshot.model.Segment
import com.onuryurtturk.heatshot.network.HeatShotResponse
import com.onuryurtturk.heatshot.network.ShotsApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class HeatShotViewModel(application: Application) : BaseViewModel(application) {

    //api object
    private val shotsApi = ShotsApiService()

    // use and dispose objects
    private val disposable = CompositeDisposable()

    //variables
    val heatShots = MutableLiveData<List<HeatShot>>()
    val shotsError = MutableLiveData<Boolean>()
    val shotsLoading = MutableLiveData<Boolean>()
    val selectedHeatShot = MutableLiveData<HeatShot>()
    var segmentHashMap = HashMap<String, Segment>()

    /**
     * api method to get data from server
     */
    fun getDataFromAPI() {
        shotsLoading.value = true
        disposable.add(
            shotsApi.getData()
                .subscribeOn(Schedulers.newThread())//asynchronous use
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<HeatShotResponse>() {
                    override fun onSuccess(t: HeatShotResponse) {
                        if (t.success){
                            heatShots.value = t.data
                        }
                        shotsLoading.value = false
                    }

                    override fun onError(e: Throwable) {
                        shotsError.value = true
                        shotsLoading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }

    /**
     * surface calculations
     */
    private fun clamp(value: Float, min: Float, max: Float): Float {
        return value * (max - min) + min
    }

    private fun clamp(value: Double, min: Double, max: Double): Double {
        return value * (max - min) + min
    }

    /**
     * Calculate points and set to map
     */
    fun calculateDataPoints() : List<HeatMap.DataPoint>{
        val resultList : ArrayList<HeatMap.DataPoint> = arrayListOf()
        calculateSegments()
        selectedHeatShot.value?.shots?.forEachIndexed { index, element ->
            if(element.segment != -1){
                //turn negative values into positive values and divide to whole area
                val posX = (element.posX.toFloat() + 7.5f)/ 15.0f
                //add blind area to each y points and divide to available area
                val posY = ((-(element.posY.toFloat())) + 1.2f) / 12.8f
                //arrange for geometrical surface
                val clampX = clamp(posX,0.0f,1.0f)
                val clampY = clamp(posY,0.0f,1.0f)
                val color  = segmentHashMap[element.segment.toString()]?.color!!
                resultList.add(HeatMap.DataPoint(clampX,clampY, clamp(30.0, 0.0, 100.0),color))
            }
        }
        return resultList
    }

    /**
     * Calculate segment totals and set to map
     */
    private fun calculateSegments(){
        selectedHeatShot.value?.shots?.forEachIndexed { _, element ->
            if(element.segment != -1){
                if(segmentHashMap.containsKey(element.segment.toString())){
                    var segment = segmentHashMap[element.segment.toString()]
                    if(element.inOut){
                        segment!!.inCount += 1
                    }else{
                        segment!!.outCount += 1
                    }
                    val rate:Double = (((segment!!.inCount * 100) / (segment.inCount + segment.outCount))).toDouble()
                    segment.rate = rate
                    segment.color = getSegmentColor(rate)
                }else{
                    segmentHashMap[element.segment.toString()] = Segment(element.segment,0,0,0)
                }
            }
        }
    }

    /**
     * Calculate total player rate
     */
    fun calculatePlayerRate() : Double{
        var totalRate  = 0.0
        for ((key, value) in segmentHashMap) {
            totalRate += value.rate
        }
        return totalRate / segmentHashMap.size
    }

    /**
     * get segment color based on success rate
     */
    fun getSegmentColor(successRate:Double) : Int {
        if(0 < successRate && successRate <= 12.5){
            return Color.rgb(178, 24, 43)
        }else if(12.5 < successRate && successRate <= 25){
            return Color.rgb(244, 109, 67)
        }else if(25 < successRate && successRate <= 37.5){
            return Color.rgb(253, 174, 97)
        }else if(37.5 < successRate && successRate <= 50){
            return Color.rgb(254, 224, 139)
        }else if(50 < successRate && successRate <= 62.5){
            return Color.rgb(230, 245, 152)
        }else if(62.5 < successRate && successRate <= 75){
            return Color.rgb(171, 221, 164)
        }else if(75 < successRate && successRate <= 87.5){
            return Color.rgb(102, 194, 165)
        }else if(87.5 < successRate && successRate <= 100){
            return Color.rgb(50, 136, 189)
        }
        return R.color.colorAccent
    }

    /**
     * disposable clear
     */
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}